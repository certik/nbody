#!/bin/bash

set -ex

gfortran -O3 -march=native -ffast-math -funroll-loops nbody.f90 -o nbody.gfortran
gcc -O3 -march=native -ffast-math -funroll-loops nbody.c -o nbody.gcc
